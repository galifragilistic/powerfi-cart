const $ = query => document.querySelector(query);
const exceptionNote = $(".buy-area__webshop .exception-note span strong.ng-star-inserted");
const buyButton = $(".buy-area__webshop button");
const productId = $("meta[itemprop='productId']").content;

let cancel = false;

const addToCart = async (productId, quantity) => {
  const rawResponse = await fetch("https://www.power.fi/api/basket/products", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ ProductId: productId, DeltaQuantity: quantity }),
  });
  const content = await rawResponse.json();

  const success = !content.Errors;

  return success;
};

const getRestrictionTime = () => {
  return exceptionNote ? exceptionNote.innerHTML : false;
};

const sleep = ms => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

function msToTime(duration) {
  var milliseconds = Math.floor((duration % 1000) / 100),
    seconds = Math.floor((duration / 1000) % 60),
    minutes = Math.floor((duration / (1000 * 60)) % 60),
    hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  return hours + ":" + minutes + ":" + seconds;
}

const waitUntil = async date => {
  console.log(`waiting until ${date}`);
  switchButton();

  let remaining = date - Date.now();

  while (!cancel) {
    if (remaining <= 0) {
      break;
    }
    await sleep(1000);
    remaining = date - Date.now();
    updateTimer(remaining);
  }
};

const switchButton = () => {
  const timerButton = $("#timerButton");
  timerButton.onclick = () => {
    cancel = true;
    timerButton.innerHTML = "Tee temput ;)";
  };
};

const setTimerButtonText = text => {
  const timerButton = $("#timerButton");
  timerButton.innerHTML = text;
};

const updateTimer = ms => {
  if (cancel) {
    return;
  }
  const text = ms > 0 ? msToTime(ms) : "Lisätään...";
  setTimerButtonText(text);
};

const leadingZero = numberStr => {
  return numberStr.length < 2 ? `0${numberStr}` : numberStr;
};

//2019-01-01T00:00:00
const parseDate = dateStr => {
  const split = dateStr.split(" ");
  const datePart = split[0].split(".");
  const timePart = split[1].split(".");

  const newDate = `${datePart[2]}-${leadingZero(datePart[1])}-${leadingZero(datePart[0])}`;
  const newTime = `${timePart[0]}:${timePart[1]}:00`;

  return new Date(`${newDate}T${newTime}`);
};

const tryAddToCart = async (id, qty) => {
  if (cancel) {
    return;
  }

  console.log(`adding to cart: ${id}`);

  const maxTime = 10 * 60000;
  const sleepFor = 500;
  let elapsed = 0;
  while (elapsed < maxTime) {
    if (await addToCart(id, qty)) {
      console.log("success!");
      setTimerButtonText("Lisätty!");
      break;
    }
    await sleep(sleepFor);
    elapsed += sleepFor;
    if (elapsed % 1000 === 0) {
      console.log(`elapsed time: ${elapsed / 1000}s`);
    }
  }
};

const start = async () => {
  const timeToBuy = getRestrictionTime();
  if (timeToBuy) {
    const date = parseDate(timeToBuy);
    const oneMinBefore = new Date(date.getTime() - 60000);
    // const test = new Date(Date.now() + 5000);

    await waitUntil(oneMinBefore);
    tryAddToCart(productId, 1);

    if (cancel) {
      cancel = false;
    }
  }
};

let btn = document.createElement("button");
btn.innerHTML = "Tee temput ;)";
btn.className = "btn btn-cyan";
btn.id = "timerButton";
btn.onclick = function () {
  start();
};
console.log({ buyButton });

buyButton.after(btn);
